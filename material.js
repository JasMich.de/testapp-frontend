//Variables
var mtElemIds=[];
var mtElems=[];
var mtLinearProgessIndicatorCounter=0;
var mtCircularProgessIndicatorCounter=0;
//MS Edge pollyfills
if (!Array.prototype.includes) {
    Object.defineProperty(Array.prototype, "includes", {
        enumerable: false,
        value: function(obj) {
            var newArr = this.filter(function(el) {
            return el == obj;
        });
        return newArr.length > 0;
        }
    });
}
//window.addEventListener("DOMContentLoaded", mtFetchTranslations);
function mtCreateApp(func) {
    mtBootFunction=func;
    mtFetchTranslations();
}
//Creating base layer (body element) for mterial Widgets
async function createMaterialHeaders(name) {
    let body = document.body;
    //create title element
    let title=document.createElement("title");
    title.innerHTML=mtLocalizeString(name);
    //Creating charset
    let charset=document.createElement("meta");
    charset.httpEquiv="Content-Type";
    charset.content="Type=text/html; charset=utf-8"
    //Creating viewport elem
    let viewport=document.createElement("meta");
    viewport.name="viewport";
    viewport.content="width=device-width, initial-scale=1, viewport-fit=cover";
    //Creating edge-meta
    let edge=document.createElement("meta");
    edge.httpEquiv="X-UA-Compatible";
    edge.content="IE=edge";
    //Appending all headers
    document.head.appendChild(title);
    document.head.appendChild(charset);
    document.head.appendChild(viewport);
    document.head.appendChild(edge);
    body.classList.add("materialApp");
    window.headersExist=true;
}
function materialApp(_params) {
    //USAGE: foo=new materialApp({
    //              name            : sting,
    //              home            : materialScaffold,
    //              mainColor       : materialColor,
    //              [ accentColor   : materialColor ]
    //});
    //expecting assoc. Array as _params
    //Setting App Name
    var app=new Object();
    app.name=_params.name;
    let body = document.body;
    if(_params.drawer!=undefined) body.appendChild(_params.drawer);
    if(_params.appBar!=undefined) body.appendChild(_params.appBar);
    //If wanted, create head elements
    createMaterialHeaders(app.name);
    //Check if path is given to navigate to
    if(mtNavigateFromUrl(location.pathname)==false) {
        mtNavigateToScaffold(_params.home);
    }
    if(_params.id==undefined) _params.id=mtGenerateId("App");
    mtElems[_params.id]=app;
    app.changeColorTheme=function(mainColor, accentColor) {
        //Setting theme colors
        let html = document.body.parentElement;
        html.style.setProperty("--main-color", mainColor.color);
        html.style.setProperty("--main-color-accent", mainColor.dark);
        html.style.setProperty("--on-main", mainColor.on);
        if(accentColor!=undefined) {
            html.style.setProperty("--accent-color", accentColor.color);
            html.style.setProperty("--accent-color-accent", accentColor.dark);
            html.style.setProperty("--on-accent", accentColor.on);
        } else {
            html.style.setProperty("--accent-color", mainColor.color);
            html.style.setProperty("--on-accent", mainColor.on);
        }
        try {
            document.getElementsByName("theme-color")[0].content=window.getComputedStyle(html).getPropertyValue("--main-color-accent");
        } catch (error) {
            console.log("Error updating theme color. theme-color meta-Tag doesn't seem to exist yet.");
        }
        try {
            mtCreateManifest({
                manifest:manifest,
                themeColor:window.getComputedStyle(html).getPropertyValue("--main-color-accent"),
                backgroundColor:window.getComputedStyle(html).getPropertyValue("--on-main"),
            });
        } catch (error) {
            console.log("No manifest created yet. Skipping updating theme color.")
        }
    }
    app.changeColorTheme(_params.mainColor, _params.accentColor);
    return(app);
}
function materialColor(_name) {
    //USAGE: foo=new materialColor(string)
    var color=new Object();
    let html = document.body.parentElement;
    const colors = [
        "google-red", "google-blue", "google-yellow", "google-green", "google-grey",
        "paper-red", "paper-pink", "paper-purple", "paper-deep-purple", "paper-indigo",
        "paper-blue", "paper-light-blue", "paper-cyan",
        "paper-teal", "paper-green", "paper-light-green",
        "paper-lime", "paper-yellow", "paper-amber",
        "paper-orange", "paper-deep-orange",
        "paper-brown", "paper-grey", "paper-blue-grey"
    ];
    var randColor=(Math.round(Math.random()*(colors.length-1)));
    if(colors.indexOf(_name)==undefined || _name==undefined) _name=colors[randColor];
    color.color = "var(--"+_name+"-500)";
    color.light = "var(--"+_name+"-300)";
    color.dark = "var(--"+_name+"-700)";
    var colorString=window.getComputedStyle(html).getPropertyValue("--"+_name+"-500");
    //Fallback colors for MS Edge
    if(colorString=="") {
        colorString="#ff5722";
    }
    if(mtColorContrastWhiteBlack(colorString)=="white") {
        color.on="var(--light-text)";
    } else {
        color.on="var(--dark-text)";
    }
    return(color);
}
function materialElement(type) {
    var elem=document.createElement(type);
    elem.getContent=function(){
        return(elem.innerHTML);
    }
    elem.setContent=function(content) {
        switch (typeof(content)) {
            case "string":
                elem.innerHTML=mtLocalizeString(content);
                break;
            case "object":
                Array.from(elem.childNodes).forEach(elem=>{
                        elem.remove();
                })
                elem.appendChild(content);
        }
    }
    return(elem);
}
function materialScaffold(_params) {
    var scaffold=new materialElement("div");
    if(_params.appBar!=undefined) scaffold.appendChild(_params.appBar);
    //Scaffold Body
    var body=new materialElement("main");
    body.classList.add("materialScaffoldBody");
    body.appendChild(_params.body);
    scaffold.appendChild(body);
    if(_params.fab!=undefined){
        _params.fab.classList.add("materialExposedFAB");
        scaffold.appendChild(_params.fab)
    };
    if(_params.footer!=undefined) scaffold.appendChild(_params.footer);
    scaffold.classList.add("materialScaffold");
    scaffold.id=_params.id;
    if(_params.id!=undefined) mtGenerateId("Scaffold");
    mtElems[_params.id]=scaffold;
    return(scaffold);
}
function materialContainer(_params) {
    var container=new materialElement("div");
    container.classList.add("materialContainer");
    if(_params.id!=undefined) container.id=_params.id;
    if(_params.children)_params.children.forEach(child => {
        container.appendChild(child);
    });
    if(_params.id==undefined) _params.id=mtGenerateId("Container");
    mtElems[_params.id]=container;
    return(container);
}
function materialProgressIndicator(_params) {
    var progressIndicator=new materialElement("div");
    progressIndicator.classList.add("materialLinearProgressBar");
    //First Bar
    var firstBar=new materialElement("div");
    firstBar.classList.add("materialLinearProgressBarPrimaryBar");
    var firstBarInner=new materialElement("span");
    firstBarInner.classList.add("materialLinearProgressBarPrimaryBarInner");
    firstBar.appendChild(firstBarInner);
    //First Bar
    var secondBar=new materialElement("div");
    secondBar.classList.add("materialLinearProgressBarSecondaryBar");
    var secondBarInner=new materialElement("span");
    secondBarInner.classList.add("materialLinearProgressBarSecondaryBarInner");
    secondBar.appendChild(secondBarInner);
    //Appending children
    progressIndicator.appendChild(firstBar);
    progressIndicator.appendChild(secondBar);
    return(progressIndicator);
}
function materialCircularProgressIndicator(_params) {
    var progressIndicator=new materialElement("div");
    progressIndicator.classList.add("materialCircularProgressIndicator");
    progressIndicator.innerHTML='<svg viewBox="22 22 44 44"><circle cx="44" cy="44" r="20.2" fill="none" stroke-width="3.6"></circle></svg>'
    return(progressIndicator);
}
function materialTextButton(_params) {
    var button=new materialElement("button");
    button.classList.add("materialButton");
    if(_params.id!=undefined) button.id=_params.id;
    if(_params.onclick!=undefined) {
        button.addEventListener("click", _params.onclick);
    }
    button.innerHTML=mtLocalizeString(_params.content);
    if(_params.id==undefined) _params.id=mtGenerateId("Button");
    mtElems[_params.id]=button;
    return(button);
}
function materialRaisedButton(_params) {
    var button=new materialTextButton(_params);
    button.classList.add("materialRaisedButton");
    return(button);
}
function materialOutlinedButton(_params) {
    var button=new materialTextButton(_params);
    button.classList.add("materialOutlinedButton");
    return(button);
}
function materialFAB(_params) {
    var content=_params.content;
    _params.content="";
    var fab=new materialTextButton(_params);
    fab.appendChild(new materialIconButton({icon:content}));
    fab.classList.add("materialFAB");
    return(fab);
}
function materialImageIcon(_params) {
    var imageIcon=new materialElement("img");
    imageIcon.src=_params.url;
    imageIcon.alt=_params.url;
    imageIcon.classList.add("materialImageIcon");
    return(imageIcon);
}
function materialTextIcon(_params) {
    var textIcon=new materialElement("span");
    textIcon.innerHTML=mtLocalizeString(_params.content);
    textIcon.classList.add("materialTextIcon");
    return(textIcon);
}

function materialIcon(text) {
    var icon=new materialElement("i");
    icon.innerHTML=text;
    icon.classList.add("material-icons");
    return(icon);
}
function materialIconButton(_params) {
    _params.content="";
    if(_params.onclick==undefined) _params.onclick=function(){return(null);};
    var button=new materialTextButton(_params);
    button.setAttribute("aria-label", "Icon Button");
    button.setContent(_params.icon);
    button.classList.add("materialIconButton");
    return(button);
}
function materialHeading(_params) {
    if(_params.type==undefined) _params.type="h1";
    var title=new materialElement(_params.type);
    title.innerHTML=mtLocalizeString(_params.title);
    title.classList.add("materialHeading");
    if(_params.id==undefined) _params.id=mtGenerateId("Heading");
    mtElems[_params.id]=title;
    return(title);
}
function materialAppBar(_params) {
    var appBar = new materialElement("header");
    appBar.classList.add("materialAppBar");
    var headerSectionContainer=new materialElement("div");
    headerSectionContainer.classList.add("materialHeaderSectionContainer");
    //Creating left content side
    var leftContent=new materialElement("div");
    leftContent.classList.add("materialHeaderSection");
    leftContent.classList.add("materialLeftHeaderSection");
    if(_params.drawerButton!=undefined) leftContent.appendChild(_params.drawerButton);
    leftContent.appendChild(_params.title);
    //Same for right side
    var rightContent=new materialElement("div");
    rightContent.classList.add("materialHeaderSection");
    rightContent.classList.add("materialRightHeaderSection");
    if(_params.actionButtons!=undefined) {
        rightContent.appendChild(new materialElement("span"));
        _params.actionButtons.forEach(button => {
            rightContent.appendChild(button);
        });
    }
    headerSectionContainer.appendChild(leftContent);
    headerSectionContainer.appendChild(rightContent);
    appBar.appendChild(headerSectionContainer);
    //Creating tabview
    if(_params.tabView!=undefined) {
        appBar.appendChild(_params.tabView);
        appBar.classList.add("materialHighAppBar");
    }
    if(_params.id==undefined) _params.id=mtGenerateId("appBar");
    mtElems[_params.id]=appBar;
    return(appBar);
}
function materialTabView(_params) {
    var tabView=new materialElement("div");
    tabView.classList.add("materialTabView");
    if(_params.id!=undefined) tabView.id=_params.id;
    _params.tabs.forEach(tab => {
        tabView.appendChild(tab);
    });
    if(_params.id==undefined) _params.id=mtGenerateId("tabView");
    mtElems[_params.id]=tabView;
    return(tabView);
}
function materialTabSection(_params) {
    var tabSection=new materialElement("span");
    tabSection.classList.add("materialTabSection");
    tabSection.innerHTML=mtLocalizeString(_params.name);
    tabSection.activity=_params.activity;
    if(_params.id==undefined) _params.id=mtGenerateId("tabSection");
    mtElems[_params.id]=tabSection;
    window.addEventListener("resize", mtResizeTabBodys);
    tabSection.addEventListener("click", function(){
        mtNavigateToTabBody(this);
    });tabSection.addEventListener("selectstart", function(){
        return(false);
    });
    return(tabSection);
}
function materialTabBody(_params) {
    var tabBody=new materialContainer(_params);
    tabBody.classList.add("materialTabBody");
    return(tabBody);
}
function materialTabBodySection(_params) {
    var tabBody=new materialElement("div");
    tabBody.classList.add("materialTabBodySection");
    var tabBodyContent=new materialElement("div");
    tabBodyContent.classList.add("materialTabBodySectionContent");
    tabBodyContent.appendChild(_params.content);
    tabBody.appendChild(tabBodyContent);
    if(_params.id==undefined) _params.id=mtGenerateId("tabBody");
    mtElems[_params.id]=tabBody;
    return(tabBody);
}
function materialDrawerButton(_params) {
    _params={};
    _params.icon="<span>☰</span>";
    var drawerButton=new materialIconButton(_params);
    drawerButton.classList.add("materialDrawerButton");
    drawerButton.addEventListener("click",mtToggleDrawer);
    return(drawerButton);
}
function materialNavigationDrawer(_params) {
    var drawerContainer=new materialElement("div");
    drawerContainer.classList.add("materialNavigationDrawerContainer");
    drawerContainer.toggle=function(state) {
        if(state==undefined||typeof(state)!="string") {
            if(this.classList.contains("shown")) {
                this.classList.remove("shown");
                setTimeout(()=> {
                    this.classList.remove("zElevated");
                }, 300);
            } else {
                this.classList.add("zElevated");
                this.classList.add("shown");
            }
        } else {
            if(state=="open") {
                this.classList.add("shown");
                this.classList.add("zElevated");
            }
            if(state=="close") {
                this.classList.remove("shown");
                setTimeout(()=> {
                    this.classList.remove("zElevated");
                }, 300);
            }
        }
    };
    drawerContainer.setPartOpen=function(px) {
        if(px>256) px=256;
        //console.log(px);
        if(this.classList.contains("shown")) {
            px=-px;
            if(px<0) px=0;
            if(px>256) px=256;
            var opacity=1-(px/256);
        } else {
            px=256-px;
            if(px<0) px=0;
            if(px>256) px=256;
            //var opacity=1-(px/256);
            var opacity=1-(px/256);
        }
        this.getElementsByClassName("materialNavigationDrawer")[0].style.right=mtPx2Rem(px)+"rem";
        this.getElementsByClassName("materialNavigationDrawerBlur")[0].style.opacity=opacity;
    }
    var drawer=new materialElement("nav");
    drawer.classList.add("materialNavigationDrawer");
    if(_params.header!=undefined) {
        var header=new materialElement("header");
        header.classList.add("materialNavigationDrawerHeader");
        header.appendChild(_params.header);
        drawer.appendChild(header);
    }
    var navigation=new materialElement("div");
    navigation.classList.add("materialNavigationDrawerList");
    navigation.appendChild(_params.content);
    drawer.appendChild(navigation);
    if(_params.footer!=undefined) {
        var footer=new materialElement("footer");
        footer.classList.add("materialNavigationDrawerFooter");
        footer.appendChild(_params.footer);
        drawer.appendChild(footer);
    }
    //Blured background
    var drawerBlur=new materialElement("div");
    drawerBlur.classList.add("materialNavigationDrawerBlur");
    /*drawerBlur.addEventListener("click", function(){
        this.parentElement.toggle();
    });*/
    //For touch support for opening and closing the drawer
    drawerBlur.addEventListener("click", function(){
        document.getElementsByClassName("materialNavigationDrawerContainer")[0].style.zIndex="";
        window.click2Close=true;
        mtToggleDrawer();
        setTimeout(() => {
            window.click2Close=undefined;
        }, 100);
    })
    document.addEventListener("touchstart", function(event){
        //Checking if touch is on drawerButton
        var touchesDrawerButton=false;
	//MS-Edge doesn't understand event.path
        if("path" in event) event.path.forEach(elem=>{
            if(elem.classList!=undefined && elem.classList.contains("materialDrawerButton")) touchesDrawerButton=true;
        });
        var beginsOnEdge=event.changedTouches[0].clientX<16;
        var largeScreen=window.innerWidth>=64*16;
        var drawerShown=document.getElementsByClassName("materialNavigationDrawerContainer")[0].classList.contains("shown");
        if((beginsOnEdge==false && drawerShown==false) || window.click2Close!=undefined || touchesDrawerButton || largeScreen) return(null);
        document.getElementsByClassName("materialNavigationDrawerContainer")[0].style.zIndex=6;
        if(event.changedTouches[0].clientX>256) {
            window.drawerToggleFirstTouchPoint=256+16;//For improved tuch point
        } else {
            window.drawerToggleFirstTouchPoint=event.changedTouches[0].clientX;//For improved tuch point
        }
        document.getElementsByClassName("materialNavigationDrawer")[0].style.transition="none";
        document.getElementsByClassName("materialNavigationDrawerBlur")[0].style.transition="none";
        document.getElementsByClassName("materialNavigationDrawerContainer")[0].setPartOpen(event.changedTouches[0].clientX-window.drawerToggleFirstTouchPoint);
        document.getElementsByClassName("materialNavigationDrawerContainer")[0].classList.add("inSlide");
    });
    document.addEventListener("touchmove", function(event){
        if(window.drawerToggleFirstTouchPoint!=undefined || window.click2Close==undefined)
            document.getElementsByClassName("materialNavigationDrawerContainer")[0].setPartOpen(event.changedTouches[0].clientX-window.drawerToggleFirstTouchPoint);
    });
    document.addEventListener("touchend", function(){
        //Resetting transitions of elements
        document.getElementsByClassName("materialNavigationDrawer")[0].style.transition="";
        document.getElementsByClassName("materialNavigationDrawerBlur")[0].style.transition="";
        setTimeout(function(){document.getElementsByClassName("materialNavigationDrawerContainer")[0].style.zIndex=""}, 300);
        //If non-valid touch, abort.
        if(window.drawerToggleFirstTouchPoint==undefined) return(null);
        //console.log(event.changedTouches[0].clientX-window.drawerToggleFirstTouchPoint);
        //Continue resetting styles
        document.getElementsByClassName("materialNavigationDrawer")[0].style.right="";
        document.getElementsByClassName("materialNavigationDrawerBlur")[0].style.opacity="";
        //Checking type of gesture
        if(event.changedTouches[0].clientX-window.drawerToggleFirstTouchPoint>128) {
            console.log("Total move more than 3rem to right. Toggling open.");
            mtToggleDrawer("open");
        } else if(window.drawerToggleFirstTouchPoint-event.changedTouches[0].clientX>128) {
            console.log("Total move more than 3rem to left. Toggling close.");
            mtToggleDrawer("close");
        } else {
            console.log("Just minor or abborted touch. Not Toggling.");
            document.getElementsByClassName("materialNavigationDrawerContainer")[0].classList.remove("inSlide");
	}
	//Resetting first touch point
        window.drawerToggleFirstTouchPoint=undefined;
    });
    //Appending children
    drawerContainer.appendChild(drawer);
    drawerContainer.appendChild(drawerBlur);
    if(_params.id==undefined) _params.id=mtGenerateId("navigationDrawer");
    mtElems[_params.id]=drawerContainer;
    return(drawerContainer);
}
function materialSnackBar(_params) {
    var snackBar=new materialElement("div");
    snackBar.classList.add("materialSnackBar");
    //Creating text content
    var textContent=new materialElement("span");
    textContent.classList.add("materialSnackBarContent");
    textContent.classList.add("materialSnackBarText");
    textContent.innerHTML=mtLocalizeString(_params.content);
    snackBar.appendChild(textContent);
    if(_params.button!=undefined) {
        var snackButton=new materialElement("span");
        snackButton.classList.add("materialSnackBarContent");
        snackButton.classList.add("materialSnackBarButton");
        _params.button.addEventListener("click", ()=>{
            mtHideSnackBar(snackBar);
        });
        snackButton.appendChild(_params.button);
        snackBar.appendChild(snackButton);
    }
    return(snackBar);
}
function materialDivider(_params) {
    var divider=new materialElement("hr");
    divider.classList.add("materialDivider");
    if(_params==undefined) _params={};
    if(_params.id==undefined) _params.id=mtGenerateId("divider");
    mtElems[_params.id]=divider;
    return(divider);
}
function materialSpacer(_params) {
    var spacer=new materialElement("div");
    spacer.classList.add("materialSpacer");
    if(_params==undefined) _params=[];
    if(_params.height!=undefined) {
        spacer.style.height=_params.height;
    } if(_params.width!=undefined) {
        spacer.style.width=_params.width;
    } else {
        spacer.style.height="1rem";
    }
    if(_params==undefined) _params={};
    if(_params.id==undefined) _params.id=mtGenerateId("spacer");
    mtElems[_params.id]=spacer;
    return(spacer);
}
function materialBox(_params) {
    var box=new materialElement("div");
    box.classList.add("materialBox");
    if(_params.children) _params.children.forEach(child => {
        box.appendChild(child);
    });
    if(_params.id==undefined) _params.id=mtGenerateId("box");
    mtElems[_params.id]=box;
    return(box);
}
function materialCardStack(_params) {
    var cardStack=new materialElement("div");
    cardStack.classList.add("materialCardStack");
    _params.children.forEach(child=>{
        cardStack.appendChild(child);
    });
    if(_params.id==undefined) _params.id=mtGenerateId("cardStack");
    mtElems[_params.id]=cardStack;
    return(cardStack);
}
function materialCard(_params) {
    var cardContainer=new materialElement("div");
    cardContainer.classList.add("materialCard");
    if(_params.onclick!=undefined) {
        cardContainer.addEventListener("click", _params.onclick);
        cardContainer.classList.add("pointer");
    }
    //Header
    var header=new materialElement("header");
    header.classList.add("materialCardHeader");
    //
    var heading=new materialHeading({
        title:_params.header,
        type:"h4"
    });
    //
    if(_params.subHeading!=undefined) {
        heading.classList.add("materialCardHeaderTitleSmall");
        var subHeading=new materialHeading({
            title:_params.subHeading,
            type:"h6"
        });
        subHeading.classList.add("materialCardHeaderSubHeading");
        //Creating containing box
        var headerBox=new materialContainer({
            children:[
                heading,
                subHeading
            ]
        });
        header.appendChild(headerBox);
    } else {
        heading.classList.add("materialCardHeaderTitle");
        header.appendChild(heading);
    }
    //Media
    if(typeof(_params.media)=="string") {
        var media=new materialElement("img");
        media.classList.add("materialCardMedia");
        media.src=_params.media;
        media.alt=_params.media;
    } else if("media" in _params && "tagName" in _params.media) {
        var media=_params.media;
    }
    //Text
    var text=new materialElement("p");
    text.classList.add("materialCardText");
    text.innerHTML=mtLocalizeString(_params.content);
    //Actions
    var actions=new materialElement("div");
    actions.classList.add("materialCardActions");
    _params.actions.forEach(action=>{
        actions.appendChild(action);
    });
    //Appending Childs
    cardContainer.appendChild(header);
    cardContainer.appendChild(media);
    cardContainer.appendChild(text);
    cardContainer.appendChild(actions);
    if(_params.id==undefined) _params.id=mtGenerateId("card");
    mtElems[_params.id]=cardContainer;
    return(cardContainer);
}
function materialListView(_params) {
    var listView=new materialElement("ul");
    listView.classList.add("materialListView");
    if(_params.seperate==true) listView.classList.add("seperate");
    _params.children.forEach(child => {
        //Sending clicks to last child element
        /*child.addEventListener("click", function(){
            this.lastChild.click();
        });*/
        listView.appendChild(child);
    });
    if(_params.id==undefined) _params.id=mtGenerateId("listView");
    mtElems[_params.id]=listView;
    return(listView);
}
function materialListItem(_params) {
    var listItem=new materialElement("li");
    listItem.classList.add("materialListItem");
    if(_params.leftContent!=undefined) {
        if(typeof(_params.leftContent)=="string") _params.leftContent=new materialTextNode({content:_params.leftContent});
        _params.leftContent.classList.add("leftContent");
        listItem.appendChild(_params.leftContent);
    }
    if(_params.centerContent!=undefined) {
        if(typeof(_params.centerContent)=="string") _params.centerContent=new materialTextNode({content:_params.centerContent});
        _params.centerContent.classList.add("centerContent");
        listItem.appendChild(_params.centerContent);
    }
    if(_params.rightContent!=undefined) {
        if(typeof(_params.rightContent)=="string") _params.rightContent=new materialTextNode({content:_params.rightContent});
        _params.rightContent.classList.add("rightContent");
        listItem.appendChild(_params.rightContent);
    }
    if(_params.id==undefined) _params.id=mtGenerateId("listItem");
    mtElems[_params.id]=listItem;
    return(listItem);
}
function materialExpandableListItem(_params) {
    var listItem=new materialElement("li");
    listItem.classList.add("materiaExpandablelListItem");
    var contentContainer=new materialElement("div");
    contentContainer.classList.add("materialExpandableListItemContaner");
    contentContainer.addEventListener("click",function(){
        if(this.classList.contains("expanded")){
            this.classList.remove("expanded");
            //this.lastChild.style.transform="rotateX(0)";
            //this.lastChild.firstChild.innerHTML="keyboard_arrow_down";
        } else {
            this.classList.add("expanded");
            //this.lastChild.style.transform="rotateX(180deg)";
            //this.lastChild.firstChild.innerHTML="keyboard_arrow_up";
        }
    });
    if(_params.leftContent!=undefined) {
        if(typeof(_params.leftContent)=="string") _params.leftContent=new materialTextNode({content:_params.leftContent});
        _params.leftContent.classList.add("leftContent");
        contentContainer.appendChild(_params.leftContent);
    }
    if(_params.centerContent!=undefined) {
        if(typeof(_params.centerContent)=="string") _params.centerContent=new materialTextNode({content:_params.centerContent});
        _params.centerContent.classList.add("centerContent");
        contentContainer.appendChild(_params.centerContent);
    }
    var rightContent=materialIconButton({
        icon:new materialIcon("keyboard_arrow_down"),
    });
    rightContent.classList.add("expandableListItemButton");
    contentContainer.appendChild(rightContent);
    listItem.appendChild(contentContainer);
    if(_params.children!=undefined) {
        var expandableContainer=new materialElement("div");
        expandableContainer.classList.add("materialExpandableListItemContent");
        if(_params.tabulate==undefined || _params.tabulate==true) expandableContainer.classList.add("tabulate");
        expandableContainer.appendChild(_params.children);
        listItem.appendChild(expandableContainer);
    }
    if(_params.id==undefined) _params.id=mtGenerateId("listItem");
    mtElems[_params.id]=listItem;
    return(listItem);
}
function materialDrawerListView(_params) {
    var drawerListView=new materialElement("ul");
    drawerListView.classList.add("materialListView");
    drawerListView.classList.add("materialDrawerListView");
    _params.children.forEach(child => {
        drawerListView.appendChild(child);
    });
    if(_params.id==undefined) _params.id=mtGenerateId("DrawerListView");
    mtElems[_params.id]=drawerListView;
    return(drawerListView);
}
function materialDrawerListItem(_params) {
    _params.rightContent=undefined;
    var drawerListItem=new materialListItem(_params);
    drawerListItem.classList.add("materialDrawerListItem");
    drawerListItem.addEventListener("click",()=>{
        if(typeof(_params.scaffold)=="object") mtNavigateToScaffold(_params.scaffold);
        if(typeof(_params.scaffold)=="function") _params.scaffold();
        //Highlighting the currently clicked listItem
        Array.from(drawerListItem.parentElement.childNodes).forEach(listItem => {
            listItem.classList.remove("highlighted");
        });
        drawerListItem.classList.add("highlighted");
    });
    return(drawerListItem);
}
function materialTextNode(_params) {
    if(_params==undefined) _params="";
    if(typeof(_params)=="string" || typeof(_params)=="number") _params={content:_params};
    var textNode=new materialElement("span");
    textNode.innerHTML=mtLocalizeString(_params.content);
    textNode.classList.add("materialTextNode");
    if(_params.id==undefined) _params.id=mtGenerateId("textNode");
    mtElems[_params.id]=textNode;
    return(textNode);
}
//materialInputs
function materialInput(_params) {
    var outerArea=new materialElement("span");
    outerArea.classList.add("materialInputOuterArea");
    var label=new materialElement("label");
    label.classList.add("materialInputLabel");
    switch (_params.type) {
        case "area":
            var input=new materialElement("textarea");
            if(_params.height!=undefined){
                input.style.minHeight=_params.height;
            } else {
                input.style.minHeight="3.375rem";
            }
            input.addEventListener("keyup",function(){
                var el = this;
                setTimeout(function(){
                    el.style.height="auto";
                    el.style.height="calc("+el.scrollHeight+"px - .5rem)";
                },0);
            })
            input.classList.add("materialInputArea")
            break;

        case "select":
            var input=new materialElement("select");
            input.classList.add("materialInputSelect");
            if(_params.label!=undefined) {
                let option=new materialElement("option");
                option.value="";
                option.innerHTML=mtLocalizeString(_params.label);
                option.disabled=true;
                option.selected=true;
                option.classList.add("materialInputSelectOption");
                input.appendChild(option);
            } else _params.label=""; //Fix for translations
            if(_params.options!=undefined) {
                var isNonAssoc=Array.isArray(_params.options);
                if(_params.force!=undefined) {
                    if(_params.force=="array") isNonAssoc=true;
                    if(_params.force=="object") isNonAssoc=false;
                }
                if(isNonAssoc) {
                    var counter=_params.options;
                } else {
                    var counter=Object.keys(_params.options);
                }
                counter.forEach(key => {
                    let option=new materialElement("option");
                    option.value=key;
                    if(isNonAssoc) {
                        option.innerHTML=key;
                    } else {
                        option.innerHTML=mtLocalizeString(_params.options[key]);
                    }
                    option.classList.add("materialInputSelectOption");
                    input.appendChild(option);
                });
            }
            break;

            case "search":
                var input=new materialElement("input");
                input.type="text";
                input.classList.add("materialInputSearch");
                var uniqueID=Date.now();
                input.setAttribute("list", uniqueID);
                var dataList=new materialElement("datalist");
                dataList.id=uniqueID;
                if(_params.options!=undefined) {
                    var isNonAssoc=Array.isArray(_params.options);
                    if(isNonAssoc) {
                        var counter=_params.options;
                    } else {
                        var counter=Object.keys(_params.options);
                    }
                    counter.forEach(key => {
                        let option=new materialElement("option");
                        option.value=key;
                        if(isNonAssoc) {
                            option.innerHTML=key;
                        } else {
                            option.innerHTML=_params.options[key];
                        }
                        option.classList.add("materialInputSearchOption");
                        dataList.appendChild(option);
                    });
                }
                outerArea.appendChild(dataList);
                break;

        default:
            var input=new materialElement("input");
            input.type=_params.type;
            break;
    }
    input.classList.add("materialInput")
    input.addEventListener("focus", function(){
        this.parentElement.childNodes[1].classList.add("floatAbove");
    })
    input.addEventListener("focusout", function(){mtInputOutFocus(this)});
    var labelText=new materialElement("span");
    labelText.classList.add("materialInputLabelText");
    labelText.innerHTML=mtLocalizeString(_params.label);
    label.appendChild(input);
    label.appendChild(labelText);
    outerArea.appendChild(label);
    if(_params.type=="select") {
        outerArea.classList.add("select");
    }
    if(_params.helper!=undefined) {
        var helper=new materialElement("span");
        helper.classList.add("materialInputHelper");
        helper.innerHTML=mtLocalizeString(_params.helper);
        outerArea.appendChild(helper);
    }
    outerArea.value=function(newValue){
        if(newValue==undefined){
            return(input.value);
        } else {
            input.value=newValue;
            mtInputOutFocus(input);
        }
    };
    if(_params.value!=undefined) {
        outerArea.value(_params.value);
    }
    if(_params.id==undefined) _params.id=mtGenerateId("outerArea");
    mtElems[_params.id]=outerArea;
    return(outerArea);
}
function materialCheckBoxSet(_params) {
    var checkBoxSet=new Object;
    checkBoxSet.id=_params.id;
    checkBoxSet.value=function() {
        var checkBoxes=document.getElementsByName(this.id);
        var checkedBoxes=new Array;
        for (let index = 0; index < checkBoxes.length; index++) {
            const box = checkBoxes[index];
            if(box.checked) checkedBoxes.push(box.value);
        }
        return(checkedBoxes);
    };
    mtElems[_params.id]=checkBoxSet;
    return(checkBoxSet);
}
function materialCheckBox(_params) {
    //Conatainer
    var checkBoxConatiner=new materialElement("label");
    checkBoxConatiner.classList.add("materialCheckBoxConatainer");
    /*checkBoxConatiner.addEventListener("click", function(){
        this.firstChild.click()
    });*/
    /*checkBoxConatiner.click=function(){
        this.children[0].click();
    }*/
    //Checkbox
    var checkBox=new materialElement("input");
    checkBox.type="checkbox";
    checkBox.classList.add("materialCheckBox");
    if(_params.checkBoxSet!=undefined) checkBox.name=_params.checkBoxSet.id;
    checkBox.value=_params.value;
    //Background
    var checkBoxBackground=new materialElement("span");
    checkBoxBackground.classList.add("materialCheckBoxBackground");
    checkBoxBackground.innerHTML='<svg version="1.1" class="materialCheckBoxBackgroundGraphic" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" xml:space="preserve"><path class="materialCheckBoxBackgroundGraphicPath" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"></path></svg>';
    checkBoxConatiner.appendChild(checkBox);
    checkBoxConatiner.appendChild(checkBoxBackground);
    checkBoxConatiner.value=function(newValue) {
        if(newValue==true||newValue==false) {
            checkBox.checked=newValue;
        } else {
            return(checkBox.checked);
        }
    }
    if(_params.id==undefined) _params.id=mtGenerateId("checkBox");
    mtElems[_params.id]=checkBoxConatiner;
    return(checkBoxConatiner);
}
function materialRadioButtonSet(_params) {
    var radioButtonSet=new Object;
    radioButtonSet.id=_params.id;
    radioButtonSet.value=function() {
        var radioButtons=document.getElementsByName(this.id);
        var buttonValue;
        for (let index = 0; index < radioButtons.length; index++) {
            const button = radioButtons[index];
            if(button.checked) buttonValue=button.value;
        }
        return(buttonValue);
    };
    mtElems[_params.id]=radioButtonSet;
    return(radioButtonSet);
}
function materialRadioButton(_params) {
    //container
    var radioButtonContainer=new materialElement("label");
    radioButtonContainer.classList.add("materialRadioButtonContainer");
    radioButtonContainer.addEventListener("click", function(){
        this.firstChild.click()
    });
    //RadioButton
    var radioButton=new materialElement("input");
    radioButton.type="radio";
    radioButton.classList.add("materialRadioButton");
    radioButton.name=_params.radioButtonSet.id;
    radioButton.value=_params.value;
    //Background
    var radioButtonBackground=new materialElement("span");
    radioButtonBackground.classList.add("materialRadioButtonBackground");
    //Background inner graphics
    var outerCircle=new materialElement("div");
    outerCircle.classList.add("materialRadioButtonBackgroundGraphicOuterCircle");
    var innerCircle=new materialElement("div");
    innerCircle.classList.add("materialRadioButtonBackgroundGraphicInnerCircle");
    radioButtonBackground.appendChild(outerCircle);
    radioButtonBackground.appendChild(innerCircle);
    radioButtonContainer.appendChild(radioButton);
    radioButtonContainer.appendChild(radioButtonBackground);
    if(_params.id==undefined) _params.id=mtGenerateId("radioButton");
    mtElems[_params.id]=radioButtonContainer;
    return(radioButtonContainer);
}
function materialInputSwitch(_params) {
    //container
    var inputSwitchContainer=new materialElement("label");
    inputSwitchContainer.classList.add("materialInputSwitchContainer");
    inputSwitchContainer.value=function() {
        var inputSwitch=this.getElementsByTagName("input")[0];
        return(inputSwitch.checked);
    }
    //inputSwitch
    var inputSwitch=new materialElement("input");
    inputSwitch.type="checkbox";
    inputSwitch.classList.add("materialInputSwitch");
    //Background
    var inputSwitchBackground=new materialElement("span");
    inputSwitchBackground.classList.add("materialInputSwitchBackground");
    //Background inner graphics
    var knob=new materialElement("div");
    knob.classList.add("materialInputSwitchKnob");
    inputSwitchBackground.appendChild(knob);
    inputSwitchContainer.appendChild(inputSwitch);
    inputSwitchContainer.appendChild(inputSwitchBackground);
    if(_params.id==undefined) _params.id=mtGenerateId("inputSwitch");
    mtElems[_params.id]=inputSwitchContainer;
    return(inputSwitchContainer);
}
//Dialogs
function materialDialog(_params) {
    var dialogContainer=new materialElement("div");
    dialogContainer.classList.add("materialDialogContainer");
    //Dialog Window
    var dialog=new materialElement("div");
    dialog.classList.add("materialDialog");
    //Content
    var question=new materialElement("div");
    question.classList.add("materialDialogQuestion");
    if(typeof(_params.question)=="string") {
        var questionParagraph=new materialElement("p");
        questionParagraph.innerHTML=mtLocalizeString(_params.question);
    } else {
        var questionParagraph=_params.question;
    }
    question.appendChild(questionParagraph);
    //Actions
    var actions=new materialElement("div");
    actions.classList.add("materialDialogActions");
    _params.actions.forEach(action=>{
        action.addEventListener("click", ()=>{
            mtHideDialog(dialogContainer);
        });
        actions.appendChild(action);
    });
    //Blured Background
    var dialogBackground=new materialElement("div");
    dialogBackground.classList.add("materialDialogBackground");
    dialogBackground.addEventListener("click",function(){
        mtHideDialog(this.parentElement);
    })
    //Appending elements
    dialog.appendChild(question);
    dialog.appendChild(actions);
    dialogContainer.appendChild(dialog);
    dialogContainer.appendChild(dialogBackground);
    return(dialogContainer);
}
//Function for managing several mtAjax requests
function materialFetchSet(_params) {
    this.requests=[];
    this.action=_params.action
    this.addRequest=function(url, handler) {
        var requests=this.requests;//For the handling function
        var action=this.action;
        this.requests[url]={
            url:url,
            handler:function(response) {//Function to be executed when finished
                requests[url].response=response;
                requests[url].status="solved";
                handler(response);
                if(requests.length==0) action();
            },
            status:"started"
        };
        mtAjax(url,this.requests[url].handler);
    }
    this.endRequest=function(request) {
        this.requests.filter(el => el !== request);
        if(requests.length==0) this.action(this);
    }
}
//UI functions
function mtPx2Rem(px) {
    return(px/16);
}
function mtNavigateToTabBody(tabSection) {
    Array.from(tabSection.parentElement.childNodes).forEach(child => {
        child.classList.remove("highlighted");
    });
    tabSection.classList.add("highlighted");
    activity=tabSection.activity;
    addClass="left";
    remClass="right";
    lastTab=activity.parentElement.getElementsByClassName("shown")[0];
    Array.from(activity.parentElement.childNodes).forEach(child=>{
        if(child==activity || child==lastTab) {
            child.classList.remove("hidden");
        } else {
            child.classList.add("hidden");
        }
        //Following command is for "timepass". Otherwise the css property doesent apply on time...
        console.log(child, window.getComputedStyle(child).display);
        child.classList.remove("shown");
        if(child==activity) {
            addClass="right";
            remClass="left";
            child.classList.add("shown");
        };
        child.classList.add(addClass);
        child.classList.remove(remClass);
    });
    mtResizeTabBodys();
}
function mtToggleDrawer(state=undefined) {
    document.getElementsByClassName("materialNavigationDrawerContainer")[0].toggle(state);
}
function mtNavigateToScaffold(scaffold,argument="") {
    //Checking if progressIndicator is shown currently
    var showIndicator=(document.getElementsByClassName("materialLinearProgressBar").length!=0);
    try {
        document.getElementsByClassName("materialNavigationDrawerContainer")[0].toggle("close");
    } catch (error) {
        console.log("Couldn't toggle drawer: "+error);
    }
    try {
        document.getElementsByClassName("materialAppBar")[0].classList.remove("materialHighAppBar");
        document.getElementsByClassName("materialAppBar")[0].removeChild(document.getElementsByClassName("materialTabView")[0]);
    } catch (error) {
        console.log("Couldn't remove TabView from AppBar: "+error);
    }
    if(scaffold!=undefined) {
        //Adding slash to argument if needed
        if((typeof(argument)=="number"||typeof(argument)=="string"||typeof(argument)=="boolean"||typeof(argument)=="bigint")==false) argument="";
        if(argument.length!=0 && (isNaN(argument)==false || argument.substr(0,1)!="/")) argument="/"+argument;
        Array.from(document.body.getElementsByClassName("materialScaffold")).forEach(oldScaffold=>{oldScaffold.remove();});
        document.body.appendChild(scaffold);
        if("createState" in window && window.createState!=false) {
            if(scaffold.id!=undefined)history.pushState({id:scaffold.id}, "ScaffoldNavigation", "/"+scaffold.id+argument);
        }
        window.createState=undefined;
        var appBars=Array.from(document.getElementsByClassName("materialAppBar"));
        appBars.forEach(function(appBar){
            appBar.classList.add("hidden");
        });
        if(appBars.length>0) appBars[appBars.length-1].classList.remove("hidden");
        //Creating a linearProgressIndicator if needed
        if(showIndicator){
            var indicator=new materialProgressIndicator();
            try {
                document.getElementsByClassName("materialScaffold")[0].insertBefore(indicator, document.getElementsByClassName("materialScaffoldBody")[0]);
            } catch (error) {
                console.log("No Scaffold defined yet, creating Progess Indicator directly into body.")
                document.body.appendChild(indicator);
            }
        }
    }
    return(null);
}
function mtNavigateFromUrl(url,createState=true) {
    if(!createState) window.createState=false;
    var scaffold=url.split("/")[1];
    if(url.split("/")[2]!="") {
        var argument=url.split("/")[2];
    } else {
        var argument=undefined;
    }
    try {
        eval(scaffold)(argument);
        return(true);
    } catch (error) {
        console.log("Unable to navigate by URL: "+error)
        return(false);
    }
}
function mtCreateManifest(_params) {
    //If just the color to be updated
    if(_params.manifest!=undefined) {
        manifest=_params.manifest;
        manifest.theme_color=_params.themeColor.trim();
        manifest.background_color=_params.backgroundColor.trim();
        var json=JSON.stringify(manifest);
        json=btoa(json);
        var links=document.getElementsByTagName("link");
        for (let index = 0; index < links.length; index++) {
            const element = links[index];
            if(element.rel!="manifest") return;
            if(element.rel=="manifest");
            var manifestLink=element;
        }
        manifestLink.href="data:text/json;base64,"+json;
    } else {
        //Checking if a pre-generated (standardized) manifest is given or mete data for generating one
        if(_params.manifestUrl) {
            var link=document.createElement("link");
            link.rel="manifest";
            link.type="text/json";
            link.href=_params.manifestUrl;
        } else {
            var appName=_params.name;
            if(_params.shortName!=undefined) {
                shortName=_params.shortName;
            } else {
                shortName=appName;
            }
            var startUrl=location.href;
            var backgroundColor=window.getComputedStyle(document.body.parentElement).getPropertyValue("--on-main").trim();
            var themeColor=window.getComputedStyle(document.body.parentElement).getPropertyValue("--main-color-accent").trim();
            //Creating icons array
            var iconsArray=new Array;
            icons=_params.icons;
            sizes=[36,48,72,96,144,192,512];
            var densitys={
                36:0.75,
                48:1.0,
                72:1.5,
                96:2.0,
                144:3.0,
                192:4.0,
                512:10.0
            }
            sizes.forEach(size=>{
                let icon={
                    src:location.protocol+"//"+location.host+_params.icons[size],
                    sizes:size+"x"+size,
                    density:densitys[size]
                }
                iconsArray.push(icon);
            });
            manifest={
                name: appName,
                short_name: shortName,
                start_url: startUrl,
                icons:iconsArray,
                display: "standalone",
                background_color: backgroundColor,
                theme_color: themeColor,
                orientation: "any"
            }
            var json=JSON.stringify(manifest);
            json=btoa(json);
            var link=document.createElement("link");
            link.rel="manifest";
            link.type="text/json";
            link.href="data:text/json;base64,"+json;
        }
        //Creating mobile app capable meta tag
        var appleMobile=document.createElement("meta");
        appleMobile.name="apple-mobile-web-app-capable";
        appleMobile.content="yes";
        var mobileApp=document.createElement("meta");
        mobileApp.name="mobile-web-app-capable";
        mobileApp.content="yes";
        //Creating chrome, safari and edge theme color tag
        var themeColor=window.getComputedStyle(document.body.parentElement).getPropertyValue("--main-color-accent").trim();
        var metas=["theme-color","msapplication-navbutton-color"]
        var metaList=[];
        metas.forEach(function(type){
            let meta=document.createElement("meta");
            meta.name=type;
            meta.content=themeColor;
            metaList.push(meta);
        })
        let meta=document.createElement("meta");
        meta.name="apple-mobile-web-app-status-bar-style";
        meta.content="black-translucent";
        metaList.push(meta);
        //Creating MS-Tile Tag
        if(_params.msTileConfig!=undefined) {
            var msTile=document.createElement("meta");
            msTile.name="msapplication-config";
            msTile.content=_params.msTileConfig;
            document.head.appendChild(msTile);
        }
        //Creating apple touch icon
        if(_params.appleTouchIcon!=undefined) {
            var appleTouch=document.createElement("link");
            appleTouch.rel="apple-touch-icon";
            appleTouch.href=_params.appleTouchIcon;
            document.head.appendChild(appleTouch);
        }
        //Creating Safari pinned tab
        if(_params.safariPinnedTab!=undefined) {
            var safariPinned=document.createElement("link");
            safariPinned.rel="mask-icon";
            safariPinned.color=_params.themeColor;
            safariPinned.href=_params.safariPinnedTab;
            document.head.appendChild(safariPinned);
        }
        var charset=document.createElement("meta");
        charset.setAttribute("charset","utf-8");
        var description=document.createElement("meta");
        description.name="description";
        description.content=_params.description;
        document.body.parentElement.lang=_params.lang;
        document.head.appendChild(appleMobile);
        document.head.appendChild(mobileApp);
        metaList.forEach(function(elem){
            document.head.appendChild(elem);
        })
        document.head.appendChild(charset);
        document.head.appendChild(description);
        document.head.appendChild(link);
        window.addEventListener('popstate', function(e){mtNavigateFromUrl(location.pathname,false)});
    }
}
function mtResizeTabBodys() {
    return(true);
    /*tabBodys=document.getElementsByClassName("materialTabBodySection");
    for (let index = 0; index < tabBodys.length; index++) {
        const tabBodySection = tabBodys[index];
        tabBodySection.style.height="calc(100% - "+(window.getComputedStyle(tabBodySection).getPropertyValue("top")+")");
    }*/
}
function mtShowSnackBar(snackBar, time=undefined) {
    if(time==undefined) time=7000;
    if(time<4000) time=4000;
    if(time>10000) time=10000;
    document.body.appendChild(snackBar);
    document.body.classList.add("snackBarShown");
    setTimeout(()=>{
        mtHideSnackBar(snackBar);
    }, time);
    setTimeout(()=>{snackBar.classList.add("shown")},1);
}
function mtHideSnackBar(snackBar) {
    snackBar.classList.remove("shown");
    document.body.classList.remove("snackBarShown");
    setTimeout(() => {
        snackBar.remove();
    }, 250);
}
function mtShowDialog(dialog) {
    document.body.appendChild(dialog);
    document.body.classList.add("dialogShown");
    setTimeout(()=>{dialog.classList.add("shown")},1);
}
function mtHideDialog(dialog) {
    dialog.classList.remove("shown");
    document.body.classList.remove("dialogShown");
    setTimeout(() => {
        dialog.remove();
    }, 250);
}
function mtShowProgressIndicator(type="linear") {
    if(type=="linear") {
        //Making cursor busy
        document.querySelector("html").classList.add("loading");
        //Recreating progressIndicator too, if document.body is it's parent
        var indicatorInBody=(document.querySelector(".materialLinearProgressBar")!=null&&document.querySelector(".materialLinearProgressBar").parentElement==document.body);
        if(mtLinearProgessIndicatorCounter<1||indicatorInBody) {
            mtLinearProgessIndicatorCounter=1;
            var indicator=new materialProgressIndicator();
            if(indicatorInBody) document.querySelector(".materialLinearProgressBar").remove();
            try {
                document.getElementsByClassName("materialScaffold")[0].insertBefore(indicator, document.getElementsByClassName("materialScaffoldBody")[0]);
            } catch (error) {
                console.log("No Scaffold defined yet, creating Progess Indicator directly into body.")
                document.body.appendChild(indicator);
            }
        } else {
            mtLinearProgessIndicatorCounter++;
            console.log("Indicator allready shown. Current number: "+mtLinearProgessIndicatorCounter);
        }
    } else {
        //Making cursor busy
        document.querySelector("html").classList.add("busy");
        //Recreating progressIndicator too, if document.body is it's parent
        var indicatorInBody=(document.querySelector(".materialCircularProgressIndicator")!=null&&document.querySelector(".materialCircularProgressIndicator").parentElement==document.body);
        if(mtCircularProgessIndicatorCounter<1||indicatorInBody) {
            mtCircularProgessIndicatorCounter=1;
            var indicator=new materialCircularProgressIndicator();
            indicator.classList.add("fullScreenProgress");
            if(indicatorInBody) document.querySelector(".materialCircularProgressIndicator").remove();
            try {
                document.getElementsByClassName("materialScaffold")[0].insertBefore(indicator, document.getElementsByClassName("materialScaffoldBody")[0]);
            } catch (error) {
                console.log("No Scaffold defined yet, creating Progess Indicator directly into body.")
                document.body.appendChild(indicator);
            }
        } else {
            mtCircularProgessIndicatorCounter++;
            console.log("Indicator allready shown. Current number: "+mtCircularProgessIndicatorCounter);
        }
    }
}
function mtHideProgressIndicator(type="linear") {
    if(type=="linear") {
        mtLinearProgessIndicatorCounter--;
        if(mtLinearProgessIndicatorCounter<1) {
            //Hiding busy cursor
            document.querySelector("html").classList.remove("loading");
            try {
                Array.from(document.getElementsByClassName("materialLinearProgressBar")).forEach(function(elem){elem.remove()});
            } catch (error) {
                console.log("No progressIndicator shown, so not removing.");
            }
        }
    } else {
        mtCircularProgessIndicatorCounter--;
        if(mtCircularProgessIndicatorCounter<1) {
            //Hiding busy cursor
            document.querySelector("html").classList.remove("busy");
            try {
                Array.from(document.getElementsByClassName("materialCircularProgressIndicator")).forEach(function(elem){elem.remove()});
            } catch (error) {
                console.log("No progressIndicator shown, so not removing.");
            }
        }
    }
}
function mtInputOutFocus(input) {
    if(input.value=="") {
        input.parentElement.childNodes[1].classList.remove("floatAbove");
    } else {
        input.parentElement.childNodes[1].classList.add("floatAbove");
    }
}
function mtGenerateId(type) {
    //mtElemIds[type] contains the current number of given ids.
    if(mtElemIds[type]==undefined) mtElemIds[type]=0;
    mtElemIds[type]++;
    //Generating materialId
    id="material"+type.substr(0,1).toUpperCase()+type.substr(1)+mtElemIds[type];
    return(id);
}
function mtTouchEvent(event) {
    //console.table(event);
    //console.log("touch");
}
function mtFetchTranslations() {
    mtStartServiceWorker();
    mtBootFunction();
    mtAjax("/translations.json", function(response) {
        mtTranslationStrings=JSON.parse(response);
        if(localStorage.displayLanguage==undefined) {
            var acceptedLanguages=new Array;
            //MS Edge pollyfill
            if(("languages" in navigator)==false) {
                navigator.languages=[navigator.language];
            }
            navigator.languages.forEach(function(language){
                //alert(language.includes("-"));
                if(language.includes("-")) language=language.substr(0, language.indexOf("-"));
                acceptedLanguages.push(language);
            });
            translationsLanguages=new Array;
            var stringNames=Object.keys(mtTranslationStrings);
            stringNames.forEach(string=>{
                Object.keys(mtTranslationStrings[string]).forEach(subStr=>{
                    if(translationsLanguages.includes(subStr)==false) translationsLanguages.push(subStr);
                });
            });
            fittingLanguage=null;
            acceptedLanguages.forEach(acceptedLanguage=>{
                if(fittingLanguage==null) translationsLanguages.forEach(language=>{
                    console.log("Offered language: "+language);
                    if(language==acceptedLanguage){
                        fittingLanguage=language;
                    }
                });
            });
            if(fittingLanguage==null) fittingLanguage=translationsLanguages[0];
            localStorage.displayLanguage=fittingLanguage;
        }
        mtLocale=new Array;
        //mtChangeLocale(localStorage.displayLanguage);
        Object.keys(mtTranslationStrings).forEach(string=>{
            mtLocale[string]=mtTranslationStrings[string][localStorage.displayLanguage];
        });
        //Going threw pending translations
        Array.from(document.querySelectorAll(".mtTranslateString")).forEach(function(transElem){
            transElem.innerHTML=mtLocalizeString("@"+transElem.dataset.string);
        })
    },false);
}
function mtLocalizeString(string) {
    try { //Getting list of translatable strings ordered by length
        var translationLabels=Object.keys(mtLocale).sort(function(a,b){return(b.length-a.length)});
        translationLabels.forEach(label=>{
            string=string.replace("@"+label, mtLocale[label]);
        });
    } catch (error) {
        //No translations available yet. Marking translatable for later
        var regexp=new RegExp("(\\W|^)(@\\w+)(\\W|$)");//(?<label>@\\w*)\\W
        var matches;
        while(matches=string.match(regexp)) {
            var label=matches[2].trim().substr(1); //Stored in second match
            string=string.replace('@'+label,'<span class="mtTranslateString" data-string="'+label+'">'+label+'</span>');
        }
    }
    return(string);
}
function mtChangeLocale(locale) {
    localStorage.displayLanguage=locale;
    location.reload();
}
async function mtAjax(url, func, async=true) {
    if("fetch" in window && (navigator.userAgent.indexOf("Edge")==-1 || /\bEdge\/(\d+)/.exec(navigator.userAgent)[1]>17)) {
        if(async) {
            fetch(url)
            .then(function(response) {
                return response.text();
            })
            .then(function(myJson) {
                func(myJson);
            });
        } else {
            var response = await fetch(url);
            func(await response.text());
        }

    } else {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {func(this.responseText)}
        };
        xhttp.open("GET", url, async);
        xhttp.send();
    }
}
function mtStartServiceWorker() {
    try {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('/sw.js')
            .then(function(reg) {
              // registration worked
              console.log('Registration succeeded. Scope is ' + reg.scope);
            }).catch(function(error) {
              // registration failed
              console.warn("Service Workers do not seem to be supported by your browser. Warning user...");
              mtWarnForNoServiceWorker();
            });
        }
    } catch (error) {
        console.warn("Service Workers do not seem to be supported by your browser. Warning user...");
        mtWarnForNoServiceWorker();
    }
}
function mtWarnForNoServiceWorker(){
    return(null);
}
function mtGetUrlLocation(url) {
    var path=url.split("/");
    if(path[0]=="") path.shift();
    if(path[path.length-1]=="") path.pop();
    return(path);
}


/**
* Bool if there is enough contrast to white
*/
function mtColorContrastWhiteBlack(hexColor){
    if(mtColorContrast2White(hexColor) >= 2.7) {//4.5
        return("white");
    } else {
        return("black");
    }
};

/**
 * Calculate contrast value to white
 */
function mtColorContrast2White(hexColor){
  var whiteIlluminance = 1;
  var illuminance = mtColorCalculateIlluminance(hexColor);
  return whiteIlluminance / illuminance;
};

function mtColorIsHex(color) {
  if(color.substr(0,1)==" ") color=color.substr(1);
  return(color.substr(0,1)=="#")
}

/**
 * Convert HEX color to RGB
 */
function mtColorHex2Rgb(hex) {
    if(hex.substr(0,1)==" ") hex=hex.substr(1);
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
};

/**
 * Calculate iluminance
 */
function mtColorCalculateIlluminance(hexColor) {
    var rgbColor = mtColorHex2Rgb(hexColor);
    var r = rgbColor.r, g = rgbColor.g, b = rgbColor.b;
    var a = [r, g, b].map(function(v) {
        v /= 255;
        return (v <= 0.03928) ?
            v / 12.92 :
            Math.pow(((v + 0.055) / 1.055), 2.4);
    });
    return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
};

//Custom Install Promt
let deferredPrompt;

window.addEventListener('beforeinstallprompt', (e) => {
	// Stash the event so it can be triggered later.
	deferredPrompt = e;
	mtShowInstallButton();
});

function mtShowInstallButton() {
	if(typeof(mtInstallPromtConfig)=="undefined") {
		mtInstallPromtConfig={
			label:"@Install",
			icon:new materialIcon("save_alt"),
			parent:document.getElementsByClassName("materialNavigationDrawer")[0],
			type:materialListItem,
			color:"var(--paper-orange-500)",
		}
	} else {
        if(!mtInstallPromtConfig.label) mtInstallPromtConfig.label="@Install";
        if(!mtInstallPromtConfig.icon) mtInstallPromtConfig.icon=new materialIcon("save_alt");
        if(!mtInstallPromtConfig.parent) mtInstallPromtConfig.parent=document.getElementsByClassName("materialNavigationDrawer")[0];
        if(!mtInstallPromtConfig.type) mtInstallPromtConfig.type=materialListItem;
        if(!mtInstallPromtConfig.color) mtInstallPromtConfig.color="var(--paper-orange-500)";
    }
    if(mtInstallPromtConfig.type==materialListItem||mtInstallPromtConfig.type==materialDrawerListItem) {
        var installPromt=mtInstallPromtConfig.type({
            id:"installPromtButton",
            leftContent:mtInstallPromtConfig.icon,
            centerContent:mtInstallPromtConfig.label,
        });
    } else {
        let buttonContent=new materialTextNode("");
        buttonContent.appendChild(mtInstallPromtConfig.icon);
        buttonContent.appendChild(new materialTextNode(" "+mtInstallPromtConfig.label));
        var installPromt=mtInstallPromtConfig.type({
            id:"installPromtButton",
            content:buttonContent,
        });
    }
	installPromt.style.background=mtInstallPromtConfig.color;
	installPromt.style.color="var(--light-text)";
	installPromt.addEventListener("click",function(){
		deferredPrompt.prompt();
		// Wait for the user to respond to the prompt
		deferredPrompt.userChoice
		.then((choiceResult) => {
			if (choiceResult.outcome === 'accepted') {
				console.log('User accepted the A2HS prompt');
			} else {
				console.log('User dismissed the A2HS prompt');
			}
			deferredPrompt = null;
		});
		mtElems.installPromtButton.remove();
	});
    installPromtIntervall=setInterval(function(){
        try{
	        document.querySelector(".materialNavigationDrawer").appendChild(installPromt);
            clearInterval(installPromtIntervall);
        } catch(error){
            //console.log("No drawer available.")
        }
    })
}
